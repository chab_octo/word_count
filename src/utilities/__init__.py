try:
    # Import for >=3.10
    from collections.abc import MutableMapping
except ImportError:
    # Import for <=3.9
    from collections import MutableMapping
