from pyspark.sql import *
from pyspark import SparkConf, SparkContext

#conf = SparkConf().setAppName('wordcount').setMaster('spark://127.0.0.1:8088')
#sc = SparkContext(conf=conf)

from pyspark.sql import SparkSession
spark = SparkSession.builder \
        .master("spark://127.0.0.1:8088") \
        .appName("wordcount") \
        .getOrCreate()


rdd = spark.sparkContext.textFile("LesMiserables.txt")
rdd = rdd.flatMap(lambda word:word.split())
rdd = rdd.map(lambda word:(word,1))
rdd = rdd.reduceByKey(lambda x,y:(x+y))
rdd.saveAsTextFile("wordcountoutput1")